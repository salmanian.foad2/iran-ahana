const express 							= require('express');
const expressSession 				= require('express-session');
const bodyParser 						= require('body-parser');
const mongoose 							= require('mongoose');
const passport 							= require('passport');
const LocalStrategy 				= require('passport-local');
const passportLocalMongoose = require('passport-local-mongoose');
const path									= require('path');
const request 							= require('request');
const cors									= require('cors');
const User 									= require('./models/user');
const House 								= require('./models/house');
const Reserve								= require('./models/reserve');
const Chat									= require('./models/chat');
const Comment 							= require('./models/comment');
const ValidationCode 				= require('./models/validationCode')

const persianMonths = {
	1: "فروردین",
	2: "اردیبهشت",
	3: "خرداد",
	4: "تیر",
	5: "مرداد",
	6: "شهریور",
	7: "مهر",
	8: "آبان",
	9: "آذر",
	10: "دی",
	11: "بهمن",
	12: "اسفند"
}

const app = express();
mongoose.connect("mongodb://localhost/iran_ahana");

app.set("view engine", "ejs");
app.set('views', path.join(__dirname, 'views'));
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(express.static("assets"));
app.use(expressSession({
	secret: "faucet dreamt line saxophone editor purplish goes slinky calculate family unharmed kilometer",
	resave: false,
	saveUninitialized: false,
	cookie: {
        secure: false,
        maxAge: 3600000 //1 hour
    }
}));
app.use(cors());

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

function isLoggedIn(req, res, next) {
	console.log("IS LOGGED IN CALLED");
	// console.log(req);
	console.log("IS AUTHENTICATED");
	console.log(req.isAuthenticated());
	if (req.isAuthenticated()) {
		return next();
	}

	res.redirect("/");
}

function persianToEnglishNumber(num) {
	const persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g];
	let res = num;

  for (var i = 0; i < 10; i++) {
    res = res.replace(persianNumbers[i], i);
  }

  return res;
}

function handleCurrency(number) {
  const numStr = number.toString();
  const repeat = Math.floor(numStr.length / 3) - 1;
  let left = (numStr.length % 3);
  let numList = [];
  let res = "";
  let counter = 1;

  if (repeat == 0) {
    counter = 0;
  }

  for (i = 1; i <= numStr.length; i++) {
    numList.push(numStr[i-1]);

    if (left > 0 && i == left) {
      numList.push(",");
    }

    if (counter > 0 && counter * 3 + left == i && counter <= repeat) {
      numList.push(",");
      counter++;
    }
  }

  for (i = 0; i < numList.length; i++) {
    res += numList[i];
  }

  return res;
}

function sendSMS(num, type, data) {
	console.log("SEND SMS CALLED");

	const number = num;
	const messageData = data;
	let message = "";

	switch (type) {
		case "validation":
			message = `کد تایید وبسایت ایران آهانا:\n${messageData.validation_code}`;
			break;

		case "reserve_request_guest":
			message = `با تشکر از حسن انتخاب و اعتماد شما\nدرخواست شما در تاریخ${messageData.register_date} با موفقیت ثبت شد.\nنتیجه تا ۲۴ ساعت آینده برای شما ارسال می‌گردد.\nبرای اطلاعات بیشتر به وبسایت ایران آهانا مراجعه کنید:\nIranAhana.com`
			break;

		case "reserve_request_host":
			message = `مالک گرامی ${messageData.owner_name}،\nشما یک درخواست رزرو ملک به کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} دارید.\nلطفا طی ۲۴ ساعت آینده از طریق وبسایت ایران آهانا نسبت به تایید یا رد این درخواست اقدام فرمایید.\nدر نظر داشته باشید پس از ۲۴ ساعت  در صورت عدم پاسخگویی مالک، درخواست بصورت خودکار رد می‌شود.\nبا تشکر\nIranAhana.com`
			break;

		case "request_accepted_guest":
			message = `کاربر گرامی\nدرخواست رزرو ملک با کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} تایید شد.\nاقامت دل‌انگیزی را برای شما آرزومندیم\nIranAhana.com`
			break;

		case "request_accepted_host":
			message = `مالک گرامی ${messageData.owner_name}،\nرزرو ملک به کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} تایید شد.\nبا تشکر\nIranAhana.com`
			break;

		case "request_denied_guest":
		message = `کاربر گرامی\nدرخواست رزرو ملک با کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} رد شد.\nبرای یافتن اقامتگاه‌های مشابه می‌توانید از وبسایت ایران آهانا دیدن فرمایید.\nIranAhana.com`
		break;
			break;

		case "request_denied_host":
			message = `مالک گرامی ${messageData.owner_name}،\nرزرو ملک به کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} رد شد.\nبا تشکر\nIranAhana.com`
			break;

		case "reserve_request_reminder":
			message = `مالک گرامی ${messageData.owner_name}،\nشما یک درخواست رزرو ملک به کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} دارید.\nلطفا طی یک ساعت آینده از طریق وبسایت ایران آهانا نسبت به تایید یا رد این درخواست اقدام فرمایید.\nدر نظر داشته باشید پس از یک ساعت  در صورت عدم پاسخگویی مالک، درخواست بصورت خودکار رد می‌شود.\nبا تشکر\nIranAhana.com`
			break;

		case "reserve_reminder_guest":
			break;

		case "reserve_reminder_host":
			break;

		case "reserve_end_reminder_guest":
			break;

		case "reserve_end_reminder_host":
			break;

		case "guest_cancel_guest":
			message = `مالک گرامی ${messageData.owner_name}،\nرزرو ملک به کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} توسط میهمان لغو شد.\nبا تشکر\nIranAhana.com`
			break;

		case "guest_cancel_host":
			message = `میهمان گرامی،\nرزرو ملک به کد ${messageData.house_id} از تاریخ ${messageData.enter_date} تا تاریخ ${messageData.exit_date} لغو شد.\nبا تشکر\nIranAhana.com`
			break;

		case "message_recieved":
			message = "کاربر گرامی، شما یک پیام جدید دارید.\nبرای مشاهده‌ی پیام به وبسایت ایران آهانا مراجعه کنید.\nبا تشکر\nIranAhana.com"

		default:
			console.log("DEFAULT SMS MESSAGE");
			break;
	}

// "from": "10009589",

	request.post({
		url: 'http://ippanel.com/api/select',
		body: {
			"op" : "send",
			"uname" : "iranmelk360",
			"pass":  "09160290559",
			"message" : message,
			"from": "3000505",
			"to" : number
		},
		json: true,
	}, function (error, response, body) {
		if (!error && response.statusCode === 200) {
			//YOU‌ CAN‌ CHECK‌ THE‌ RESPONSE‌ AND SEE‌ ERROR‌ OR‌ SUCCESS‌ MESSAGE
			// console.log(response.body);
			console.log("SMS SENT SUCCESSFULY");
			// console.log(newValidationCode);
		} else {
			console.log("THERE WAS AN ERROR");
			console.log(error);
		}
	});
}

function reminder(resData) {
	const reminderType = resData.type;
	const reserveId = resData.reserveId;

	switch (reminderType) {
		case "request-23":
			//Check the status
			//If waiting send reminder message to owner
			break;

		case "request-24":
			//Check the status
			//If waiting set to declined and send messages to owner and guest
			break;

		case "remind-24":
			//Check the status
			//If accepted send reminder to owner and guest
			break;

		case "remind-1":
			//Check the status
			//If active send reminder to owner and guest
			break;

		default:
			console.log("DEFAULT REMINDER");
			break;
	}
}

function dateToPersian(dateRaw) {
	let result = {};
	const date = new Date(dateRaw);
	const day = Date(date).toLocalDateString("fa-IR").slice(-2).replace("/", "");
	const month = Date(date).toLocalDateString("fa-IR").slice(5, 7).replace("/", "");
	const year = Date(date).toLocalDateString("fa-IR").slice(0, 4);

	result["day"] = persianToEnglishNumber(day);
	result["month"] = persianMonths[persianToEnglishNumber(month)];
	result["year"] = persianToEnglishNumber(year);

	return result;
}

function randomGenerator(digits) {
	let digits = parseInt(digits);
	let result = Math.round(Math.random() * (10 * digits));

	if (result.toString().length < digits) {
		result = result * 10;
	}

	return result;
}

function handleRequestError(err, res, errText) {
	console.log(errText);
	console.log(err);
	res.status(200).send({
		status: 'error',
		message: err
	})
}

app.post("/send_validation", async (req, res) => {
	console.log("SIGN UP POST CALLED");

	let userExists = false;
	let validationExists = false;
	let newValidationCode = randomGenerator(6);

	await User.find({
		username: req.headers.cellnumber
	}, (err, user) => {
		if (err) {
			handleRequestError(err, res, 'USER FIND ERROR');
			return;
		};

		if (user.length != 0) {
			userExists = true;
		}
	});

	if (userExists) {
		handleRequestError('user_exists', res, 'USER EXISTS')
		return;
	};

	await ValidationCode.find({
		cell_number: req.headers.cellnumber
	}, (err, code) => {
		if (err) {
			handleRequestError('server_error', res, 'CHECKING VALIDATION EXISTANCE ERROR')
			return;
		}

		if (code.length == 0) {
			validationExists = false;
		} else {
			validationExists = true;
		}
	});

	if (validationExists) {
		ValidationCode.updateOne({
			cell_number: req.headers.cellnumber
		},
		{
			$set: {
				validation_code: {
					code: newValidationCode,
					valid_until: new Date().setHours(new Date().getHours() + 1)
				}
			}
		},
		(err, code) => {
			if (err) {
				handleRequestError('server_error', res, 'VALIDATION CREATION ERROR');
				return;
			}

			sendSMS(req.headers.cellnumber, "validation", {validation_code: newValidationCode});

	    res.status(200).send({
	    	status: "success",
	    	message: "code_sent"
	    });
		})
	} else {
		ValidationCode.create({
			cell_number: req.headers.cellnumber,
			validation_code: {
				code: newValidationCode,
				valid_until: new Date().setHours(new Date().getHours() + 1)
			}
		}, (err, code) => {
			if (err) {
				handleRequestError('server_error', res, 'VALIDATION CREATION ERROR');
				return;
			}

			sendSMS(code["cell_number"], "validation", {validation_code: code["validation_code"]["code"]});

	    res.status(200).send({
	    	status: "success",
	    	message: "code_sent"
	    });
		})
	}
}) //REFACTORED

app.post("/validate", (req, res) => {
	//req includes cell number and validation code
	console.log("VALIDATE POST CALLED");

	ValidationCode.find({
		cell_number: req.headers.cellnumber
	}, (err, vCode) => {
		if (err || vCode.length == 0) {
			handleRequestError('validation_not_exist', res, 'FINDING VALIDATION ERROR');
			return;
		}

		const { valid_until, code } = vCode[0]['validation_code']

		if (new Date() > valid_until) {
			handleRequestError('late_validation', res, 'LATE VALIDATION');
			return;
		}

		if (code == req.headers.validationcode) {
			console.log("USER VALIDATED");
			res.status(200).send({
				status: "success",
				message: "user_validated"
			})
		} else {
			handleRequestError('wrong_code', res, 'BAD CODE');
		}
	})
}) //REFACTORED

app.post("/register_user", async function (req, res) {
	console.log("REGISTER USER CALLED");

	let userValidated = true;
	let userId = randomGenerator(8);

	await ValidationCode.find({
		cell_number: req.body.username
	}, (err, vCode) => {
		if (err) {
			handleRequestError('server_error', res, 'CHECKING VALIDATION EXISTANCE ERROR');
			return;
		}

		if (vCode.length == 0) {
			userValidated = false;
		} else {
			const { valid_until, code } = vCode[0]['validation_code'];

			if (
				code == req.body.validationcode &&
				new Date() < valid_until
			) {
				userValidated = true;
			} else {
				userValidated = false;
			}
		}
	})

	if (!userValidated) {
		handleRequestError('signup_not_valid', res, null);
		return;
	}

	const newUser = new User({
		username: req.body.username,
		cell_number: req.body.username,
		email: "",
		first_name: "",
		last_name: "",
		social_id: 0,
		social_card_img_id: "",
		profile_pic_img_id: "",
		reserves: [],
		houses: [],
		favorites: [],
		messages: [],
		history: [],
		role: "",
		user_id: userId.toString()
	});

	User.register(newUser,
	req.body.password,
	(err, user) => {
		if (err) {
			handleRequestError('register_error', res, 'USER REGISTER ERROR');
			return;
		}

		passport.authenticate("local")(req, res, () => {
			console.log("USER AUTHENTICATED");
			res.status(200).send({
				status: "success",
				message: "user_registered"
			})
		});
	})
}) // REFACTORED

app.post("/login", passport.authenticate("local"), (req, res) => {
	console.log("AUTHENTICATION CHECK SUCCESSFUL");
	res.status(200).send({
		status: "success",
		message: "authenticated"
	})
}) //REFACTORED

app.post("/checklogin", (req, res) => {
	console.log("CHECK LOGIN CALLED");
	let response = req.isAuthenticated();
	console.log(response);
	res.status(200).send({
		status: "no idea",
		losginstatus: response
	})
})

app.post("/forgot_password", async function (req, res) {
	console.log("FORGOT PASSWORD POST CALLED");

	let userExists = true;
	let validationExists = true;
	let newValidationCode = randomGenerator(6);

	await User.find({
		username: req.body.cellnumber
	}, (err, user) => {
		if (err) {
			handleRequestError(err, res, 'USER FIND ERROR');
			return;
		}

		if (user.length == 0) {
			userExists = false;
		}
	});

	if (userExists) {
		ValidationCode.updateOne({
			cell_number: req.body.cellnumber
		},
		{
			$set: {
				validation_code: {
					code: newValidationCode,
					valid_until: new Date().setHours(new Date().getHours() + 1)
				}
			}
		},
		(err, code) => {
			if (err) {
				handleRequestError('server_error', res, 'VALIDATION CREATION ERROR');
				return;
			}

			sendSMS(req.body.cellnumber, "validation", {validation_code: newValidationCode});

	    return res.status(200).send({
	    	status: "success",
	    	message: "code_sent"
	    });
		})
	} else {
		handleRequestError('user_not_exist', res, null);
		return;
	};
}) //REFACTORED

app.post("/forgot_password_check", async function (req, res) {
	console.log("FORGPT PASSWORD CHECK POST CALLED");

	let userExists = true;

	await User.find({
		username: req.body.cellnumber
	}, (err, user) => {
		if (err) {
			handleRequestError(err, res, 'USER FIND ERROR');
			return;
		}

		if (user.length == 0) {
			userExists = false;
		}
	});

	if (!userExists) {
		handleRequestError('request_invalid', res, null);
	}

	ValidationCode.find({
		cell_number: req.body.cellnumber
	}, (err, vCode) => {
		if (err || vCode.length == 0) {
			handleRequestError('validation_not_exist', res, 'FINDING VALIDATION ERROR');
			return;
		}

		const { valid_until, code } = vCode[0]['validation_code'];

		if (new Date() > valid_until) {
			handleRequestError('late_validation', res, 'LATE VALIDATION');
			return;
		}

		if (code == req.body.validationcode) {
			console.log("USER VALIDATED");
			res.status(200).send({
				status: "success",
				message: "user_validated"
			})
		} else {
			handleRequestError('wrong_code', res, null);
		}
	})
}) //REFACTORED

app.post("/edit_password", async function (req, res) {
	console.log("EDIT PASSWORD POST CALLED");

	if (!req.isAuthenticated()) {
		let userExists = true;
		let validated = true;

		await User.find({
			username: req.body.cellnumber
		}, (err, user) => {
			if (err) {
				handleRequestError(err, res, 'USER FIND ERROR');
				return;
			}

			if (user.length == 0) {
				userExists = false;
			}
		});

		if (!userExists) {
			handleRequestError('request_invalid', res, null);
			return;
		}

		await ValidationCode.find({
			cell_number: req.body.cellnumber
		}, (err, vCode) => {
			if (err || vCode.length == 0) {
				handleRequestError('validation_not_exist', res, 'FINDING VALIDATION ERROR');
				return;
			}

			const { valid_until, code } = code[0]['validation_code'];

			if (
				new Date() < valid_until ||
				code != req.body.validationcode
			) {
				validated = false;
			}
		})

		if (!validated) {
			handleRequestError('request_invalid', res, null);
			return;
		}

		User.findByUsername(req.body.cellnumber).then(user => {
	    user.setPassword(req.body.password, () => {
	      user.save();

	      res.status(200).send({
	      	status: "success",
	      	message: 'password_edited'
	      });
	    });
		}, err => {
			handleRequestError('edit_error', res, 'CHANGE PASSWORD ERROR');
		})
	} else {
		User.findByUsername(req.user.username).then(user => {
	    user.setPassword(req.body.password, () => {
	      user.save();

	      res.status(200).send({
	      	status: "success",
	      	message: 'password_edited'
	      });
	    });
		}, err => {
			handleRequestError('edit_error', res, 'CHANGE PASSWORD ERROR');
		})
	}
}) //REFACTORED

app.post("/profile", isLoggedIn, (req, res) => {
	console.log("PROFILE POST CALLED");

	User.updateOne({
			cell_number: req.user.username
		},
		{
			$set: req.body
		},
		(err, data) => {
			if (err) {
				handleRequestError('server_error', res, 'UPDATE USER ERROR');
				return;
			}

			res.status(200).send({
				status: "success",
				message: "data_saved",
				data: data
			})
		}
	)
}) //REFACTORED

app.post("/register", isLoggedIn, async function (req, res) {
	console.log("REGISTER POST CALLED");

	const stage = req.body.stage;

	switch (stage) {
		case "init":
			console.log("HOUSE INIT");
			const tempPicsArray = [
				"https://www.voro.ca/wp-content/uploads/2019/07/white-modern-house-curved-patio-archway-c0a4a3b3.jpg",
				"https://cdn.archilovers.com/projects/58e2ebbf-9d94-4085-850a-990fc6f60e2e.jpg",
				"https://freshome.com/wp-content/uploads/2018/09/contemporary-exterior.jpg"
			];
			let houseId = randomGenerator(8);

			await House.create({
				pics: tempPicsArray,
				title: "",
				region: "",
				city: "",
				village: "",
				address: "",
				building_kind: "",
				house_kind_options: {},
				about_house: "",
				bedrooms: 0,
				house_area: 0,
				ground_area: 0,
				standard_space: 0,
				maximum_space: 0,
				single_bed: 0,
				double_bed: 0,
				texture: {},
				about_texture: "",
				facilities: {},
				about_facilities: "",
				enter_time: "",
				exit_time: "",
				minimum_reserve_number: 0,
				can_party: false,
				can_bring_pets: false,
				custom_rules: "",
				normal_price: 0,
				weekend_price: 0,
				peak_price: 0,
				extra_price: 0,
				discount_1: {
					more_than: 0,
					discount: 0
				},
				discount_2: {
					more_than: 0,
					discount: 0
				},
				house_id: houseId.toString(),
				owner_id: req.user.user_id,
				reserve_history: [],
				comments: [],
				reserved_dates: [],
				active: false,
				data_complete: false,
				rating: 0,
				ratingsNumber: 0
			}, (err, house) => {
				if (err) {
					handleRequestError('server_error', res, 'HOUSE INIT ERROR');
					return;
				}
			})

			await User.updateOne({
				username: req.user.username
			}, {
				$set: {
					houses: [...req.user.houses, houseId]
				}
			}, (err, result) => {
				if (err) {
					handleRequestError('server_error', res, 'USER UPDATE ERROR');
					return;
				}

				res.status(200).send({
					status: "success",
					message: "house_initialized",
					house_id: houseId
				})
			})
			break;

		case "update":
			console.log("REGISTER UPDATE STAGE");

			House.updateOne({
				house_id: req.body.house_id
			},
			{
				$set: req.body.payload
			},
			(err, data) => {
				if (err) {
					handleRequestError('server_error', res, 'HOUSE UPDATE ERROR');
					return;
				}

				res.status(200).send({
					status: "success",
					message: "house_updated"
				})
			})
			break;

		default:
			console.log("REGISTER POST DEFAULT");
			break;
	}
}) //REFACTORED

app.post("/sendmessage", isLoggedIn, async function (req, res) {
	console.log("SEND MESSAGE POST");

	let messageObj = {};
	let receiverId;
	let receiver;
	let chat;
	let notifId = randomGenerator(8);

	let notifObj = {
		notif_id: notifId,
		type: "message_recieved",
		title: "پیام جدید",
		content: "کاربر گرامی، شما یک پیام جدید دارید.",
		dismissable: "true"
	}

	await Chat.find({
		chat_id: req.body.chat_id
	}, (err, foundChat) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING CHAT');
			return;
		}

		chat = foundChat[0];
	})

	receiverId = req.user.username == chat.user1 ? chat.user2 : chat.user1;

	messageObj["sender"] = req.user.username;
	messageObj["time"] = new Date();
	messageObj["text"] = req.body.text;
	messageObj["read"] = false;
	messageObj["order"] = parseInt(chat.counter);

	await Chat.updateOne({
		chat_id: req.body.chat_id
	}, {
		$set: {
			counter: parseInt(chat.counter) + 1,
			messages: [
				...chat.messages,
				messageObj
			]
		}
	}, (err, updatedChat) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING CHAT');
			return;
		}
	})

	await User.find({
		username: receiverId
	}, (err, user) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDIGN RECEIVER USER');
			return;
		}

		receiver = user[0];
	})

	sendSMS(receiver.cell_number, "message_recieved", null);

	await User.updateOne({
		username: receiverId
	}, {
		$set: {
			notifications: [
				...receiver.notifications,
				notifObj
			]
		}
	}, (err, updatedUser) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING RECEIVER USER');
			return;
		}
	})

	res.status(200).send({
		"status": "success",
		"message": "message_sent",
		"messageOrder": messageObj.order
	})
}) //REFACTORED

app.post("/updatemn", isLoggedIn, async function (req, res) {
	console.log("UPDATE MESSAGES AND NOTIFICATIONS POST");

	let user = req.user;
	let notifications = req.user.notifications;
	let messages = [];
	let unreadChatFound = false;
	let userTemp = "";
	let unreadTemp = 0;

	for (i = 0; i < user.messages.length; i++) {
		await Chat.find({
			chat_id: user.messages[i]
		}, (err, chat) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR FINDING CHAT');
				return;
			}

			if (chat[0].messages.length > 0) {
				for (i = 0; i < chat[0].messages.length; i++) {
					if (chat[0].messages[i].sender != req.user.username && chat[0].messages[i].read == false) {
						unreadTemp += 1;
					}
				}

				userTemp = chat[0].user1 == req.user.username ? chat[0].user2 :  chat[0].user1;

				User.find({
					username: userTemp
				}, (err, user) => {
					if (err) {
						handleRequestError('server_error', res, 'ERROR FINDING OTHER USER');
						return;
					}

					chat[0].messages.sort((a, b) => (a.order > b.order) ? 1 : -1);

					messages.push({
						chatData: chat[0],
						otherUserData: user[0],
						unreadMessages: unreadTemp
					})

					if (!unreadChatFound && unreadTemp > 0) {
						unreadChatFound = true;
					}

					unreadTemp = 0;
				})
			}
		})
	}

	if (!unreadChatFound) {
		messages = [];
	}

	return res.status(200).send({
		"status": "success",
		"message": "update_successful",
		"notifs": notifications,
		"messages": messages
	})
}) //REFACTORED

app.post("/checkmessages", isLoggedIn, async function (req, res) {
	console.log("CHECK MESSAGES POST");

	let chat;
	let newMessages = [];

	await Chat.find({
		chat_id: req.body.chat_id
	}, (err, foundChat) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING CHAT');
			return;
		}

		chat = foundChat[0];

		if (!(parseInt(chat.counter) - 1 == parseInt(req.body.last_order))) {
			chat.messages.map((message) => {
				if (parseInt(message.order) > parseInt(req.body.last_order)) {
					newMessages.push(message);
				}
			})
		}
	})

	return res.status(200).send({
		"status": "success",
		"messages": newMessages
	})
}) //REFACTORED

app.post("/setread", isLoggedIn, async function (req, res) {
	console.log("SET READ POST");

	let chatMessages;

	await Chat.find({
		chat_id: req.body.chat_id
	}, (err, foundChat) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING CHAT');
			return;
		}

		chatMessages = foundChat[0].messages;

		chatMessages.map((message) => {
			if (message.user != req.user.username) {
				message.read = true;
			}
		})
	})

	await Chat.updateOne({
		chat_id: req.body.chat_id
	}, {
		$set: {
			messages: chatMessages
		}
	}, (err, updatedChat) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING CHAT');
			return;
		}

		res.status(200).send({
			"status": "success",
			"message": "chat_updated"
		})
	})
}) //REFACTORED

app.post("/getchat", isLoggedIn, async function (req, res) {
	console.log("GET CHAT POST");

	let chat;
	let user1 = req.user;
	let user2;
	let chats = [];
	let chatId = randomGenerator(8);


	await Chat.find({
		$or: [
			{
				user1: req.user.username,
				user2: req.body.receiver_id
			}, {
				user1: req.body.receiver_id,
				user2: req.user.username
			}
		]
	}, (err, foundChat) => {
		if (err) {
			handleRequestError('server_error', res, 'GETTING CHAT ERROR');
			return;
		}

		if (foundChat.length != 0) {
			chat = foundChat[0]
		}
	})

	await User.find({
		username: req.body.receiver_id
	}, (err, foundUser) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING RECEIVER USER');
			return;
		}

		user2 = foundUser[0];
	})

	if (chat) {
		return res.status(200).send({
			"status": "success",
			"message": "chat_found",
			"chat": chat,
			"receiver_data": user2
		})
	} else {
		await Chat.create({
			chat_id: chatId,
			user1: req.user.username,
			user2: req.body.receiver_id,
			counter: 0,
			messages: []
		}, (err, newChat) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR CREATING NEW CHAT');
				return;
			}

			chat = newChat;
		})

		await User.updateOne({
			username: user1.username
		}, {
			$set: {
				messages: [
					...user1.messages,
					chat.chat_id
				]
			}
		}, (err, updatedUser) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR UPDATING USER');
				return;
			}
		})

		await User.updateOne({
			username: user2.username
		}, {
			$set: {
				messages: [
					...user2.messages,
					chat.chat_id
				]
			}
		}, (err, updatedUser) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR UPDATING RECEIVER USER');
				return;
			}
		})

		return res.status(200).send({
			"status": "success",
			"message": "chat_created",
			"chat": chat,
			"receiver_data": user2
		})
	}
}) //REFACTORED

app.post("/dismissnotif", isLoggedIn, async function (req, res) {
	console.log("DISMISS NOTIFICATION POST");

	let notifs = req.user.notifications;
	let notifIndex;

	notifs.forEach((notif, i) => {
		if (notif.notif_id == req.body.notif_id) {
			notifIndex = i;
		}
	})

	if (notifIndex != undefined) {
		notifs.splice(notifIndex, 1);

		await User.updateOne({
			username: req.user.username
		}, {
			$set: {
				notifications: notifs
			}
		}, (err, updatedUser) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR UPDATING USER');
				return;
			}

			res.status(200).send({
				"status": "success",
				"message": "notif_dismissed"
			})
		})
	} else {
		handleRequestError('notif_not_found', res, null);
	}
}) //REFACTORED

app.post("/rate", isLoggedIn, async function (rqe, res) {
	console.log("RATE POST");

	const average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
	let commentData = req.body;
	let commentId = randomGenerator(8);
	let comment;
	let house;
	let reserve;

	await Comment.create({
		comment_id: commentId,
		reserve_id: commentData.reserve_id,
		user_id: req.user.username,
		house_id: commentData.house_id,
		comment: commentData.comment,
		rate: commentData.rate
	}, (err, newComment) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR CREATING COMMENT');
			return;
		}

		comment = newComment;
	})

	await House.find({
		house_id: commentData.house_id
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING HOUSE');
			return;
		}

		house = result[0]
	})

	house.ratings.push(parseInt(commentData.rate));
	house.rating = average(house.ratings);
	house.ratingsNumber = house.ratingsNumber + 1;

	await House.updateOne({
		house_id: house.house_id
	}, {
		$set: {
			ratings: house.ratings,
			rating: house.rating,
			ratingsNumber: house.ratingsNumber,
			comments: [
				...house.comments,
				comment.comment_id
			]
		}
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING HOUSE');
			return;
		}
	})

	await Reserve.updateOne({
		reserve_id: reserve.reserve_id
	}, {
		$set: {
			comment: comment.comment_id
		}
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING RESERVE');
			return;
		}
	})

	return res.status(200).send({
		"status": "success",
		"message": "comment_submited"
	})
}) //REFACTORED

app.post("/houseactivation", isLoggedIn, async function (req, res) {
	console.log("HOUSE ACTIVATION POST");

	let houseActive = req.body.action == "activate" ? true : false;
	let canDeactivate = true;

	if (houseActive == false) {
		await House.find({
			house_id: req.body.house_id
		}, (err, result) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR FINDING HOUSE ACTIVATION');
				return;
			}

			if (result[0].reserved_dates.length > 0) {
				canDeactivate = false;
			}
		})
	}

	if (canDeactivate) {
		House.updateOne({
			house_id: req.body.house_id
		}, {
			$set: {
				active: houseActive
			}
		}, (err, result) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR UPDATING HOUSE ACTIVATION');
				return;
			}

			return res.status(200).send({
				"status": "success",
				"message": "house_updated"
			})
		})
	} else {
		handleRequestError('can_not_deactivate', res, null);
		return res.status(200).send({
			"status": "error",
			"message": "can_not_deactivate"
		})
	}
}) //REFACTORED

app.post("/handlerequest", isLoggedIn, async function (req, res) {
	console.log("HANDLE REQUEST POST");

	let reserveStatus = req.body.action == "accept" ? "accepted" : "declined";
	let reserve;
	let owner;
	let house;

	await Reserve.find({
		reserve_id: req.body.reserve_id
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING RESERVE');
			return;
		}

		reserve = result[0];
	})

	await House.find({
		house_id: reserve.house_id
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING HOUSE');
			return;
		}

		house = result[0]
	})

	await User.find({
		username: house.owner_id
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING OWNER');
			return;
		}

		owner = result[0];
	})

	await Reserve.updateOne({
		reserve_id: req.body.reserve_id
	}, {
		$set: {
			status: reserveStatus
		}
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING RESERVE STATUS');
			return;
		}
	})

	if (req.body.action == "accept") {
		//Add dates to house reserved_dates
		await House.updateOne({
			house_id: house.house_id
		}, {
			$set: {
				reserved_dates: [
					...house.reserved_dates,
					{
						enter_date: reserve.enter_date,
						exit_date: reserve.exit_date
					}
				]
			}
		}, (err, result) => {
			if (err) {
				handleRequestError('server_error', res, 'ERROR UPDATING HOUSE RESERVED DATES');
				return;
			}
		})

		sendSMS(owner.cell_number, "request_accepted_host", {
			owner_name: owner.last_name,
			house_id: house.house_id,
			enter_date: reserve.enter_date,
			exit_date: reserve.exit_date
		})

		sendSMS(owner.cell_number, "request_accepted_host", {
			owner_name: owner.last_name,
			house_id: house.house_id,
			enter_date: reserve.enter_date,
			exit_date: reserve.exit_date
		})
		//Set time out to activate reserve
		//Set time out to end reserve
	} else {
		//Notify host
		//Notify guest
	}
}) //REFACTORED

app.get("/logout", (req, res) => {
	req.logout();
	res.redirect("/");
}) //REFACTORED

app.get("/", async function (req, res) {
	let carouselItems = [
		{
			name: "تهران",
			imgUrl: "images/tehran.jpg"
		},
		{
			name: "متل قو",
			imgUrl: "images/motelGhoo.jpg"
		},
		{
			name: "شیراز",
			imgUrl: "images/shiraz.jpg"
		},
		{
			name: "یزد",
			imgUrl: "images/yazd.jpg"
		},
		{
			name: "اصفهان",
			imgUrl: "images/isfahan.jpg"
		}
	];

	for (i = 0; i < carouselItems.length; i++) {
		await House.find({
			city: carouselItems[i].name
		}, (err, result) => {
			if (err || result.length == 0) {
				console.log("FIND CITY HOUSES ERROR OR 0 HOUSES");
				carouselItems[i].spots = 0;
			} else {
				carouselItems[i].spots = results.length;
			}
		})
	}

	res.render("./landing_page", {
		currentUser: req.user,
		pageData: {
			home: "active"
		}
	});
}) //REFACTORED

app.get("/dashboard/profile", isLoggedIn, (req, res) => {
	res.render("./profile", {
		currentUser: req.user,
		pageData: {
			profile: "active"
		}
	});
}) //REFACTORED

app.get("/dashboard", isLoggedIn, (req, res) => {
	res.redirect("/dashboard/profile");
}) //REFACTORED

app.get("/dashboard/guest", isLoggedIn, async function (req, res) {
	let reserves = [];
	let history = [];
	let favorites = [];
	let reserveTemp = {};
	let historyTemp = {};
	let favoriteTemp;

	await User.find({
		username: req.user.username
	}, (err, user) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING USER');
			return;
		}

		user[0].reserves.map((reserve) => {
			Reserve.find({
				reserve_id: reserve
			}, (err, reserveObj) => {
				if (err) {
					handleRequestError('server_error', res, 'ERROR GETTING RESERVED DATA');
					return;
				}

				House.find({
					house_id: reserveObj[0].house_id
				}, (err, house) => {
					if (err) {
						handleRequestError('server_error', res, 'ERROR GETTING RESERVED HOUSE');
						return;
					}

					reserveTemp.from_date_object = dateToPersian(reserveObj[0].enter_date);
					reserveTemp.to_date_object = dateToPersian(reserveObj[0].exit_date);

					reserves.push({
						house_data: house[0],
						reserve_data: {
							reserve_from_date_day: reserveTemp.from_date_object.day,
							reserve_from_date_month: reserveTemp.from_date_object.month,
							reserve_from_date_year: reserveTemp.from_date_object.year,
							reserve_to_date_day: reserveTemp.to_date_object.day,
							reserve_to_date_month: reserveTemp.to_date_object.month,
							reserve_to_date_year: reserveTemp.to_date_object.year,
							reserve_price: handleCurrency(reserveObj[0].price),
							reserve_status: reserveObj[0].status
						}
					})
				})
			})
		});

		user[0].history.map((reserveId) => {
			Reserve.find({
				reserve_id: reserveId
			}, (err, reserveObj) => {
				if (err) {
					handleRequestError('server_error', res, 'ERROR GETTING HISTORY OBJECT');
					return;
				}

				House.find({
					house_id: reserveObj[0].house_id
				}, (err, house) => {
					if (err) {
						handleRequestError('server_error', res, 'ERROR GETTING HISTORY HOUSE');
						return;
					}

					historyTemp.from_date_object = dateToPersian(reserveObj[0].enter_date);
					historyTemp.to_date_object = dateToPersian(reserveObj[0].exit_date);

					history.push({
						house_data: house[0],
						history_data: {
							history_from_date_day: historyTemp.from_date_object.day,
							history_from_date_month: historyTemp.from_date_object.month,
							history_from_date_year: historyTemp.from_date_object.year,
							history_to_date_day: historyTemp.to_date_object.day,
							history_to_date_month: historyTemp.to_date_object.month,
							history_to_date_year: historyTemp.to_date_object.year,
							history_price: handleCurrency(reserveObj[0].price)
						}
					})
				})
				//ADD USER RATING AND USER COMMENT
			})
		});

		user[0].favorites.map((houseId) => {
			House.find({
				house_id: houseId
			}, (err, house) => {
				if (err) {
					handleRequestError('server_error', res, 'ERROR GETTING FAVORITE');
					return;
				}

				favoriteTemp = house[0];
				favoriteTemp["normal_price"] = handleCurrency(favoriteTemp["normal_price"]);
				favorites.push(favoriteTemp);
			})
		})
	})

	res.render("./guest_dashboard", {
		currentUser: req.user,
		pageData: {
			guestDashboard: "active",
			background: "no_img",
			reserves: reserves,
			history: history,
			favorites: favorites
		}
	});
}) //REFACTORED

app.get("/dashboard/host", isLoggedIn, async function (req, res) {
	let requests = [];
	let guests = [];
	let houses = [];
	let requestTemp = {};
	let houseTemp;

	await User.find({
		username: req.user.username
	}, (err, user) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING USER');
			return;
		}

		user[0].reserve_requests.map((reserveId) => {
			Reserve.find({
				reserve_id: reserveId
			}, (err, reserveObj) => {
				if (err) {
					handleRequestError('server_error', res, 'ERROR GETTING RESERVED DATA');
					return;
				}

				House.find({
					house_id: reserveObj[0].house_id
				}, (err, house) => {
					if (err) {
						handleRequestError('server_error', res, 'ERROR GETTING REQUEST HOUSE');
						return;
					}

					requestTemp.from_date_object = dateToPersian(reserveObj[0].enter_date);
					requestTemp.to_date_object = dateToPersian(reserveObj[0].exit_date);

					if (reserveObj[0].status == "active") {
						guests.push({
							house_data: house[0],
							reserve_data: {
								reserve_from_date_day: requestTemp.from_date_object.day,
								reserve_from_date_month: requestTemp.from_date_object.month,
								reserve_from_date_year: requestTemp.from_date_object.year,
								reserve_to_date_day: requestTemp.to_date_object.day,
								reserve_to_date_month: requestTemp.to_date_object.month,
								reserve_to_date_year: requestTemp.to_date_object.year,
								reserve_price: handleCurrency(reserveObj[0].price),
								reserve_status: reserveObj[0].status
							}
						})
					} else {
						requests.push({
							house_data: house[0],
							reserve_data: {
								reserve_from_date_day: requestTemp.from_date_object.day,
								reserve_from_date_month: requestTemp.from_date_object.month,
								reserve_from_date_year: requestTemp.from_date_object.year,
								reserve_to_date_day: requestTemp.to_date_object.day,
								reserve_to_date_month: requestTemp.to_date_object.month,
								reserve_to_date_year: requestTemp.to_date_object.year,
								reserve_price: handleCurrency(reserveObj[0].price),
								reserve_status: reserveObj[0].status
							}
						})
					}
				})
			})
		});

		user[0].houses.map((houseId) => {
			House.find({
				house_id: houseId
			}, (err, house) => {
				if (err) {
					handleRequestError('server_error', res, 'ERROR GETTING HOUSE');
					return;
				}

				houseTemp = house[0];
				houseTemp["normal_price"] = handleCurrency(houseTemp["normal_price"]);
				houses.push(houseTemp);
			})
		})
	})

	res.render("./host_dashboard", {
		currentUser: req.user,
		pageData: {
			hostDashboard: "active",
			background: "no_img",
			requests: requests,
			guests: guests,
			houses: houses
		}
	});
}) //REFACTORED

app.get("/dashboard/notifications", isLoggedIn, async function (req, res) {
	console.log("NOTIFICATIONS AND MESSAGES");

	let userNotifs = req.user.notifications;
	let profileComplete = true;
	let hasProfileNotif = false;
	let updateUser = false;
	let notifIndex = 0;
	let chats = [];
	let chatTemp;
	let userTemp;
	let unreadTemp = 0;
	let notifId = randomGenerator(8)

	let completeProfileNotif = {
		notif_id: notifId,
		type: "complete_profile",
		title: "تکمیل پروفایل",
		content: "لطفا نسبت به تکمیل اطلاعات پروفایل خود اقدام فرمایید",
		dismissable: "false"
	}

	if (
		req.user.first_name == "" ||
		req.user.last_name == ""
	) {
		profileComplete = false;
	}

	userNotifs.forEach((notif, i) => {
		if (notif.type == "complete_profile") {
			hasProfileNotif = true;
			notifIndex = i;
		}
	});

	if (!profileComplete && !hasProfileNotif) {
		userNotifs.push(completeProfileNotif);
		updateUser = true;
	} else if (profileComplete && hasProfileNotif) {
		userNotifs.splice(notifIndex, 1);
		updateUser = true;
	}

	if (updateUser) {
		await User.updateOne({
			username: req.user.username
		}, {
			$set: {
				notifications: userNotifs
			}
		}, (err, updated) => {
			if (err) {
				console.log("ERROR UPDATING USER NOTIFS");
				console.log(err);

				return res.sendStatus(404);
			}
		})
	}

	req.user.messages.map(async function (chatId) {
		await Chat.find({
			chat_id: chatId
		}, (err, chat) => {
			if (err) {
				console.log("ERROR FINDING CHAT");
				console.log(err);

				res.sendStatus(404);
			}

			for (i = 0; i < chat[0].messages.length; i++) {
				if (chat[0].messages[i].sender != req.user.username && chat[0].messages[i].read == false) {
					unreadTemp += 1;
				}
			}

			if (chat[0].messages.length > 0) {
				chatTemp = chat[0];
				userTemp = chatTemp.user1 == req.user.username ? chatTemp.user2 :  chatTemp.user1;

				User.find({
					username: userTemp
				}, (err, user) => {
					if (err) {
						console.log("ERROR FINDING OTHER USER");
						console.log(err);

						res.sendStatus(404);
					}

					chats.push({
						chatData: chatTemp,
						otherUserData: user[0],
						unreadMessages: unreadTemp
					})

					unreadTemp = 0;
				})
			}
		})
	})

	res.render("./notifications_and_messages", {
		currentUser: req.user,
		pageData: {
			notifications: "active",
			background: "no_img",
			userNotifs: userNotifs,
			chats: chats
		}
	});
}) //REFACTORED

app.get("/register", isLoggedIn, (req, res) => {
	res.render("./register_house", {
		currentUser: req.user,
		pageData: {
			currentPage: "register_house",
			background: "no_img"
		}
	});
}) //REFACTORED

app.get("/house/:houseid", async function (req, res) {
	let house;
	let isLiked = "";

	if (req.isAuthenticated()) {
		if (req.user.favorites.includes(req.params.houseid)) {
			isLiked = "liked";
		}
	}

	await House.find({
		house_id: req.params.houseid
	}, (err, result) => {
		if (err || result.length == 0) {
			console.log("FIND HOUSE ERROR");
			console.log(err);

			return res.sendStatus(404);
		}

		house = result[0];
		res.render("./show_house", {
			currentUser: req.user,
			pageData: {
				background: "no_img",
				house: house,
				houseLiked: isLiked
			}
		});
	})
}) //REFACTORED

app.post("/like", (req, res) => {
	console.log("HOUSE POST LIKE");

	let favorites;

	if (req.isAuthenticated()) {
		if (req.body.action == "like") {
			User.updateOne({
				username: req.user.username
			}, {
				$set: {
					favorites: [...req.user.favorites, req.body.houseid]
				}
			}, (err, result) => {
				return res.status(200).send({
					status: "success",
					message: "house_liked"
				})
			})
		} else {
			favorites = req.user.favorites;
			favorites.splice(favorites.indexOf(req.body.houseid), 1);

			User.updateOne({
				username: req.user.username
			}, {
				$set: {
					favorites: favorites
				}
			}, (err, result) => {
				return res.status(200).send({
					status: "success",
					message: "house_liked"
				})
			})
		}
	} else {
		return res.status(401).send("user_unauthorized");
	}
}) //REFACTORED

app.post("/reserve", isLoggedIn, async function (req, res) {
	console.log("RESERVE POST CALLED");

	let house;
	let owner;
	let reserve;
	let ownerName;
	let enterDateText;
	let exitDateText;
	let enterDateObj;
	let exitDateObj;
	let houseAvailable = true;
	let user = req.user;
	let reserveData = req.body.reserve_data;
	let userAvailable = true;
	let registerDate = new Date();
	let reserveId = randomGenerator(8);

	await House.find({
		house_id: req.body.houseid
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING HOUSE');
			return;
		}

		house = result[0];
	})

	if (!house.active) {
		handleRequestError('house_unavailable', res, null);
		return;
	}

	if (user.houses.includes(house.house_id)) {
		handleRequestError('house_host_user', res, null);
		return;
	}

	house.reserved_dates.map((dateObj) => {
		if (
			(
				reserveData.enter_date > dateObj.enter_date &&
				reserveData.enter_date < dateObj.exit_date
			) ||
			reserveData.enter_date == dateObj.enter_date ||
			(
				reserveDate.exit_date > dateObj.enter_date &&
				reserveData.exit_date < dateObj.exit_date
			)
		) {
			houseAvailable = false;
		}
	})

	if (!houseAvailable) {
		handleRequestError('reserved_already', res, 'HOUSE RESERVED');
		return;
	}

	user.reserves.map((dateObj) => {
		if (
			(
				reserveData.enter_date > dateObj.enter_date &&
				reserveData.enter_date < dateObj.exit_date
			) ||
			reserveData.enter_date == dateObj.enter_date ||
			(
				reserveDate.exit_date > dateObj.enter_date &&
				reserveData.exit_date < dateObj.exit_date
			)
		) {
			userAvailable = false;
		}
	})

	if (!userAvailable) {
		handleRequestError('user_has_reserve_already', res, 'USER NOT AVAILABLE');
		return;
	}

	await User.find({
		user_id: house.owner_id
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR FINDING OWNER');
			return;
		}

		owner = result[0];
		ownerName = owner.last_name != "" ? owner.last_name : "";
	})

	await Reserve.create({
		reserve_id: reserveId,
		house_id: house.house_id,
		guest_id: user.user_id,
		enter_date: reserveData.enter_date,
		exit_date: reserveData.exit_date,
		price: parseInt(reserveData.price),
		guests: parseInt(reserve_data.guests),
		status: "waiting"
	}, (err, reserveObj) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR CREATING RESERVE');
			return;
		};

		reserve = reserveObj;
	})

	await User.updateOne({
		username: user.username
	}, {
		$set: {
			reserves: [
				...user.reserves,
				reserveId
			]
		}
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING GUEST USER');
			return;
		}
	});

	await User.updateOne({
		user_id: house.owner_id
	}, {
		$set: {
			reserve_requests: [
				...owner.reserve_requests,
				reserveId
			]
		}
	}, (err, result) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR UPDATING OWNER');
			return;
		}

		//SET TIME OUT FOR REMINDER IN 23 HOURS
		setTimeout(reminder.bind(null, {
			type: "request-23",
			reserve_id: reserveId
		}), 1000 * 60 * 60 * 23);
		//SET TIME OUT FOR CHECK IN 24 HOURS
		setTimeout(reminder.bind(null, {
			type: "request-24",
			reserve_id: reserveId
		}), 1000 * 60 * 60 * 24);
		//SEND SMS TO OWNER AND GUEST
		enterDateObj = dateToPersian(reserveData.enter_date);
		exitDateObj = dateToPersian(reserveData.exit_date);
		enterDateText = `${enterDateObj.day} ${enterDateObj.month} ${enterDateObj.year}`
		exitDateText = `${exitDateObj.day} ${exitDateObj.month} ${exitDateObj.year}`

		sendSMS(owner.cell_number, "reserve_request_host", {
			register_date: registerDate
		});
		sendSMS(user.cell_number, "reserve_request_guest", {
			owner_name: ownerName,
			house_id: house.house_id,
			enter_date: enterDateText,
			exit_date: exitDateText
		});


		res.status(200).send({
			status: "success",
			message: "request_sent"
		})
	})
}) //REFACTORED

app.get("/search/:destination/:from/:to/:guests", async function (req, res) {
	console.log("SEARCH GET");

	let housesListRaw = [];
	let housesListFilteredFirst = [];
	let housesListFilteredSecond = [];
	let housesListFinal = [];
	let dateTemp;
	let dateObjTemp;

	if (req.params.destination.toLowerCase() != "all") {
		await House.find({
			city: req.params.destination
		}, (err, houses) => {
			if (err) {
				console.log("FINDING HOUSES BY CITY ERROR");
				console.log(err);

				return res.sendStatus(404);
			}

			houses.map((house) => {
				if (house.status == "active") {
					housesListRaw.push(house);
				}
			})
		})
	} else {
		await House.find({}, (err, houses) => {
			if (err) {
				console.log("FINDING HOUSES ERROR");
				console.log(err);

				return res.sendStatus(404);
			}

			houses.map((house) => {
				if (house.status == "active") {
					housesListRaw.push(house);
				}
			})
		})
	}

	if (housesListRaw.length != 0) {
		if (req.params.guests != "all") {
			housesListRaw.map((house) => {
				if (parseInt(req.params.guests) <= house.maximum_space) {
					housesListFilteredFirst.push(house);
				}
			})
		} else {
			housesListFilteredFirst = housesListRaw;
		}
	};

	if(housesListFilteredFirst.length != 0) {
		if (req.params.from != "all") {
			dateTemp = new Date(req.params.from);

			if (dateTemp != "Invalid Date") {
				housesListFilteredFirst.map((house) => {
					house.reserved_dates.map((dateObj) => {
						dateObjTemp.enter_date = new Date(dateObj.enter_date);
						dateObjTemp.exit_date = new Date(dateObj.exit_date);
						if (
							!(
								dateTemp > dateObjTemp.enter_date &&
								dateTemp < dateObjTemp.exit_date
							) &&
							!dateTemp == dateObjTemp.enter_date
						) {
							housesListFilteredSecond.push(house);
						}
					})
				})
			} else {
				housesListFilteredSecond = housesListFilteredFirst;
			}
		} else {
			housesListFilteredSecond = housesListFilteredFirst;
		}
	}

	if (housesListFilteredSecond.length != 0) {
		if (req.params.to != "all") {
			dateTemp = new Date(req.params.to);

			if (dateTemp != "Invalid Date") {
				housesListFilteredSecond.map((house) => {
					house.reserved_dates.map((dateObj) => {
						dateObjTemp.enter_date = new Date(dateObj.enter_date);
						dateObjTemp.exit_date = new Date(dateObj.exit_date);
						if (
							!(
								dateTemp > dateObjTemp.enter_date &&
								dateTemp < dateObjTemp.exit_date
							) &&
							!dateTemp == dateObjTemp.enter_date
						) {
							housesListFinal.push(house);
						}
					})
				})
			} else {
				housesListFinal = housesListFilteredSecond;
			}
		} else {
			housesListFinal = housesListFilteredSecond;
		}
	}

	if (housesListFinal.length != 0 && req.isAuthenticated()) {
		for (i = 0; i < housesListFinal.length; i++) {
			if (req.user.favorites.includes(housesListFinal[i].house_id)) {
				housesListFinal[i]["liked"] = true;
			} else {
				housesListFinal[i]["liked"] = false;
			}
		}
	}

	res.render("./search", {
		currentUser: req.user,
		pageData: {
			background: "no_img",
			houses: housesListFinal,
			filters: {
				destination: req.params.destination,
				from: req.params.from,
				to: req.params.to,
				guests: req.params.guests
			}
		}
	});
}) //REFACTORED

app.get("/search", (req, res) => {
	res.redirect("/search/all/all/all/all")
}) //REFACTORED

app.post("/search", async function (req, res) {
	console.log("SEARCH POST");

	let filterObj = {};
	let filterList = ["maximum_space", "bedrooms", "price_min", "price_max", "building_kind", "city", "region", "can_bring_pets", "can_party"];
	let priceMinTemp = 0;
	let priceMaxTemp = 0;
	let addHouse = false;
	let placeKindKeysTemp = [];
	let optionsKeysTemp = [];
	let bedNum = 0;
	let filteredHouses = [];

	for (i = 0; i < filterList.length; i++) {
		if (req.body[filterList[i]] != "all") {
			if (filterList[i] == "maximum_space" || filterList[i] == "bedrooms") {
				filterObj[filterList[i]] = {
					$gte: parseInt(req.body[filterList[i]])
				}
			} else if (filterList[i] == "price_min") {
				priceMinTemp = parseInt(req.body[filterList[i]])
			} else if (filterList[i] == "price_max") {
				priceMaxTemp = parseInt(req.body[filterList[i]])
			} else if (filterList[i] == "building_kind") {
				filterObj["building_kind"] = {
					$in: req.body["building_kind"]
				}
			} else {
				filterObj[filterList[i]] = req.body[filterList[i]]
			}
		}
	};

	if (priceMinTemp != 0 && priceMaxTemp != 0) {
		filterObj["normal_price"] = {
			$gte: priceMinTemp,
			$lte: priceMaxTemp
		}
	} else if (priceMinTemp != 0 && priceMaxTemp == 0) {
		filterObj["normal_price"] = {
			$gte: priceMinTemp
		}
	} else if (priceMinTemp == 0 && priceMaxTemp != 0) {
		filterObj["normal_price"] = {
			$lte: priceMaxTemp
		}
	};

	await House.find(filterObj, (err, houses) => {
		if (err) {
			handleRequestError('server_error', res, 'ERROR GETTING FILTER HOUSES');
			return;
		}

		houses.map((house) => {
			placeKindKeysTemp = Object.keys(house.texture);
			optionsKeysTemp = Object.keys(house.facilities);
			bedNum = parseInt(house.single_bed) + parseInt(house.double_bed);

			addHouse = false;

			if (req.body.bed_num <= bedNum) {
				addHouse = true;
			};

			if (req.body.place_kind[0] != "all" && addHouse) {
				placeKindKeysTemp.forEach((kind) => {
					if (req.body.place_kind.includes(kind)) {
						addHouse = true;
						break;
					} else {
						addHouse = false;
					}
				});
			} else {
				addHouse = true;
			}

			if (req.body.options[0] != "all" && addHouse) {
				optionsKeysTemp.forEach((option) => {
					if (req.body.place_kind.includes(option)) {
						addHouse = true;
						break;
					} else {
						addHouse = false;
					}
				});
			} else {
				addHouse = true;
			}

			if (addHouse) {
				filteredHouses.push(house);
			}
		})
	})

	res.status(200).send({
		houses: filteredHouses
	})
}) //REFACTORED

app.get("/us", (req, res) => {
	res.render("./about_us", {
		currentUser: req.user
	});
}) //REFACTORED

app.get("/terms", (req, res) => {
	res.render("./terms_and_conditions", {
		currentUser: req.user
	});
}) //REFACTORED

const port = process.env.PORT || 4050;

app.listen(port, () => {
	console.log("Server is listening on port: ", port);
})
