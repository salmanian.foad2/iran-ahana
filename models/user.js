const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

const userSchema = mongoose.Schema({
	username: Number,
	cell_number: Number,
	password: String,
	email: String,
	first_name: String,
	last_name: String,
	social_id: String,
	social_card_img_id: String,
	profile_pic_img_id: String,
	reserves: Array,
	houses: Array,
	favorites: Array,
	messages: Array,
	history: Array,
	role: String,
	user_id: String,
	reserve_requests: Array,
	notifications: Array
})

userSchema.plugin(passportLocalMongoose);

const User = mongoose.model("User", userSchema);

module.exports = User;
