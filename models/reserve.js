const mongoose = require('mongoose');

const reserveSchema = mongoose.Schema({
	reserve_id: String,
	house_id: String,
	guest_id: String,
	enter_date: String,
	exit_date: String,
	price: Number,
	guests: Number,
	status: String,
	comment: String
})

const Reserve = mongoose.model("Reserve", reserveSchema);

module.exports = Reserve;
