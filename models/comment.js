const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
	comment_id: String,
	reserve_id: String,
	user_id: String,
	house_id: String,
	comment: String,
	rate: String
})

const Comment = mongoose.model("Comment", commentSchema);

module.exports = Comment;
