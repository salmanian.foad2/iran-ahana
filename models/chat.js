const mongoose = require('mongoose');

const chatSchema = mongoose.Schema({
	chat_id: String,
	user1: String,
	user2: String,
	counter: Number,
	messages: Array
})

const Chat = mongoose.model("Chat", chatSchema);

module.exports = Chat;
