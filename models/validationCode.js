const mongoose = require('mongoose');

const validationCodeSchema = mongoose.Schema({
	cell_number: Number,
	validation_code: Object
})

const ValidationCode = mongoose.model("ValidationCode", validationCodeSchema);

module.exports = ValidationCode;
